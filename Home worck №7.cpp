﻿#include <iostream>
#include <array>

using namespace  std;

struct Animal
{
 virtual void voice()const = 0;
 virtual~Animal(){}
};

class Dog:virtual public Animal{

  void voice()const override {
   puts("Wolf!");
  }
  
};
class Cat:virtual public Animal {
    void voice()const override {
        puts("Meow!");
    }
};
class Duck :virtual public Animal {
    void voice()const override {
        puts("Quack!");
    }
    
};

int main()
{
 Dog a;
 Cat c;
 Duck e;
 array<Animal*, 3>animals{ &a, &c, &e };
 for (const auto& pa:animals)pa->voice();
 system("pause >nul");



}

